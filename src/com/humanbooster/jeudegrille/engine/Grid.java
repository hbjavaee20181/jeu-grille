package com.humanbooster.jeudegrille.engine;

import com.humanbooster.jeudegrille.exception.GameException;
import com.humanbooster.jeudegrille.model.Cell;
import com.humanbooster.jeudegrille.model.Direction;
import com.humanbooster.jeudegrille.model.Position;

/**
 * Created by Ben on 31/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Grid {

    private final int gridSize;
    private Cell[][] cells;

    public Grid(int gameSize) {
        this.gridSize = gameSize;
        cells = new Cell[gameSize][gameSize];
    }

    public void init() {

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                cells[x][y] = Cell.newEmptyCell();
            }
        }
    }

    public void paint() {
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                Cell cell = cells[x][y];
                System.out.print(cell.getType().getRepresentation());
            }
            System.out.println();
        }
    }

    public void updateElement(Cell cell, Position position) {
        cells[position.getX()][position.getY()] = cell;
    }

    public Position getCellPosition(Cell cell) {
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (cells[x][y].equals(cell)) {
                    return Position.of(x, y);
                }
            }
        }
        throw new GameException();
    }

    public Cell getCellAtPosition(Position position) {
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (x == position.getX() && y == position.getY()) {
                    return cells[x][y];
                }
            }
        }
        return null;
    }

    public boolean canMoveTo(Position position, Direction dir) {
        int nextPoint = position.getAxis(dir.getAxis()) + dir.getOperation();
        if (nextPoint < 0 || nextPoint > gridSize - 1) {
            return false;
        }
        return true;
    }
}
