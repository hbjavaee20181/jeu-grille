package com.humanbooster.jeudegrille.engine;

import com.humanbooster.jeudegrille.constants.Constants;
import com.humanbooster.jeudegrille.exception.GameFunctionException;
import com.humanbooster.jeudegrille.model.Cell;
import com.humanbooster.jeudegrille.model.CellType;
import com.humanbooster.jeudegrille.model.MenuOption;
import com.humanbooster.jeudegrille.utils.GridUtils;

/**
 * Created by Ben on 31/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class GameEngine {

    private static final GameEngine ENGINE = new GameEngine();

    public static GameEngine getInstance() {
        return ENGINE;
    }

    private Cell character;
    private Grid grid;
    private boolean isLaunched = true;
    private BackgroundEngineThread engineThread;

    private GameEngine() {
        resetGrid();
    }

    private void resetGrid() {
        grid = new Grid(Constants.GRID_SIZE);
        character = new Cell(CellType.CHARACTER);

        grid.init();
        grid.updateElement(character, GridUtils.createRandom(Constants.GRID_SIZE));

        // Start engine thread
        engineThread = new BackgroundEngineThread();
        engineThread.start();
    }

    public void repaint() {
        clear();
        grid.paint();
        Console.printMenu();
    }

    public void scanUserInputAndExecute() {
        try {
            MenuOption menuOption = Console.getMenuOption();
            menuOption.getGameFunction().execute(grid, character);
        } catch (GameFunctionException e) {
            Console.println(e.getMessage());
        }
    }

    public boolean isLaunched() {
        return isLaunched;
    }

    public void stopGame() {
        engineThread.interrupt();
        try {
            System.out.println("Waiting Engine Thread to stop ...");
            engineThread.join();
        } catch (InterruptedException e) {
            Console.println("Error while stopping background engine thread : " + e.getMessage());
        }
        isLaunched = false;
    }

    private void clear() {
        // Just simulate a console clear (yeah ... that's dirty i know :()
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }
}
