package com.humanbooster.jeudegrille.engine.functions;

import com.humanbooster.jeudegrille.engine.Grid;
import com.humanbooster.jeudegrille.model.*;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class GoToLeftFunction extends GameFunction{
    @Override
    public void run(Grid grid, Cell cell, Position position, Direction direction) {
        grid.updateElement(Cell.newEmptyCell(), position);
        grid.updateElement(cell, position.decrX());
    }

    @Override
    public Direction getDirection() {
        return Direction.LEFT;
    }
}
