package com.humanbooster.jeudegrille.engine.functions;

import com.humanbooster.jeudegrille.engine.Grid;
import com.humanbooster.jeudegrille.exception.CellCannotMoveException;
import com.humanbooster.jeudegrille.model.Cell;
import com.humanbooster.jeudegrille.model.Direction;
import com.humanbooster.jeudegrille.model.Position;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public abstract class GameFunction {

    public final void execute(Grid grid, Cell cell) {
        if (getDirection().equals(Direction.NONE)) {
            run(null, null, null, null);
            return;
        }
        Position position = grid.getCellPosition(cell);
        if (grid.canMoveTo(position, getDirection())) {
            run(grid, cell, position, getDirection());
        } else {
            throw new CellCannotMoveException();
        }
    }

    public abstract void run(Grid grid, Cell cell, Position position, Direction direction);

    public abstract Direction getDirection();
}
