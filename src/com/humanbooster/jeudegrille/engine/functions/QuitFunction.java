package com.humanbooster.jeudegrille.engine.functions;

import com.humanbooster.jeudegrille.engine.Console;
import com.humanbooster.jeudegrille.engine.GameEngine;
import com.humanbooster.jeudegrille.model.Cell;
import com.humanbooster.jeudegrille.model.Direction;
import com.humanbooster.jeudegrille.engine.Grid;
import com.humanbooster.jeudegrille.model.Position;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class QuitFunction extends GameFunction {

    @Override
    public void run(Grid grid, Cell cell, Position position, Direction direction) {
        Console.println("Aurevoir !");
        GameEngine.getInstance().stopGame();
    }

    @Override
    public Direction getDirection() {
        return Direction.NONE;
    }
}
