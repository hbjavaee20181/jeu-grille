package com.humanbooster.jeudegrille.engine;

import com.humanbooster.jeudegrille.constants.Constants;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class BackgroundEngineThread extends Thread {


    @Override
    public void run() {
        GameEngine engine = GameEngine.getInstance();
        while (!Thread.interrupted() && GameEngine.getInstance().isLaunched()) {
            try {
                engine.repaint();
                Thread.sleep(Constants.GAME_REFRESH_RATE);
            } catch (InterruptedException e) {
                Console.println("Stopping Background Engine thread ...");
                break;
            }
        }
        Console.println("Background Engine Thread stopped !");
    }
}
