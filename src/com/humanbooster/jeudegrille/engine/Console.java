package com.humanbooster.jeudegrille.engine;

import com.humanbooster.jeudegrille.exception.UnknownUserInputException;
import com.humanbooster.jeudegrille.model.MenuOption;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class Console {

    public static MenuOption getMenuOption() {
        String s = new Scanner(System.in).nextLine();
        try {
            return MenuOption.fromUserInput(s);
        } catch (UnknownUserInputException e) {
            Console.println("Option inconnue. Retry !");
            return getMenuOption();
        }
    }

    public static void println(String str) {
        System.out.println(str);
    }

    public static void printMenu() {
        String options = Stream.of(MenuOption.values())
                .map(o -> o.getUserInput() + " (" + o.name() + ")")
                .collect(Collectors.joining(" / "));
        System.out.println("---------------------------------------");
        System.out.println("Que voulez-vous faire ? " + options);
        System.out.println("---------------------------------------");
    }
}
