package com.humanbooster.jeudegrille.constants;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class Constants {

    private Constants() {
    }

    public static final long GAME_REFRESH_RATE = 1000L;

    public static final int GRID_SIZE = 20;
}
