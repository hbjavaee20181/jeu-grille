package com.humanbooster.jeudegrille.exception;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class CellCannotMoveException extends GameFunctionException {

    public CellCannotMoveException() {
        super("La cellule ne peut pas bouger à cet endroit !");
    }
}
