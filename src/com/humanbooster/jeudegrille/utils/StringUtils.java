package com.humanbooster.jeudegrille.utils;

import java.util.UUID;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class StringUtils {

    private StringUtils() {

    }

    public static final String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }
}
