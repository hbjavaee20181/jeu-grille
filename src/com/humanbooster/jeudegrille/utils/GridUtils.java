package com.humanbooster.jeudegrille.utils;

import com.humanbooster.jeudegrille.model.Position;

import java.util.Random;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class GridUtils {

    private GridUtils() {

    }

    public static Position createRandom(int gridSize) {
        Random rand = new Random();
        int x = rand.nextInt(gridSize);
        int y = rand.nextInt(gridSize);
        return new Position(x, y);
    }
}
