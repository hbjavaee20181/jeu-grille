package com.humanbooster.jeudegrille.model;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum Axis {
    X,
    Y
}
