package com.humanbooster.jeudegrille.model;

import com.humanbooster.jeudegrille.engine.functions.*;
import com.humanbooster.jeudegrille.exception.UnknownUserInputException;

import java.util.stream.Stream;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum MenuOption {
    GO_TOP("T", new GoToTopFunction()),
    GO_BOTTOM("B", new GoToBottomFunction()),
    GO_LEFT("L", new GoToLeftFunction()),
    GO_RIGHT("R", new GoToRightFunction()),
    QUIT("Q", new QuitFunction());

    private GameFunction gameFunction;
    private String userInput;

    MenuOption(String userInput, GameFunction gameFunction) {
        this.userInput = userInput;
        this.gameFunction = gameFunction;
    }

    public String getUserInput() {
        return userInput;
    }

    public GameFunction getGameFunction() {
        return gameFunction;
    }

    public static MenuOption fromUserInput(String userInput) {
        return Stream.of(values())
                .filter(o -> o.getUserInput().equalsIgnoreCase(userInput))
                .findFirst()
                .orElseThrow(UnknownUserInputException::new);
    }
}
