package com.humanbooster.jeudegrille.model;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Pair<E, T> {

    public static <E, T> Pair<E, T> of(E left, T right) {
        Pair<E, T> pair = new Pair<>();
        pair.left = left;
        pair.right = right;
        return pair;
    }

    private E left;
    private T right;


    public E getLeft() {
        return left;
    }

    public T getRight() {
        return right;
    }
}
