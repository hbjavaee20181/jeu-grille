package com.humanbooster.jeudegrille.model;

/**
 * Created by Ben on 01/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum Direction {
    TOP(Axis.Y, -1),
    BOTTOM(Axis.Y, 1),
    RIGHT(Axis.X, 1),
    LEFT(Axis.X, -1),
    NONE(null, 0);

    private int operation;
    private Axis axis;

    Direction(Axis axis, int operation) {
        this.axis = axis;
        this.operation = operation;
    }

    public int getOperation() {
        return operation;
    }

    public Axis getAxis() {
        return axis;
    }

}
