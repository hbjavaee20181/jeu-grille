package com.humanbooster.jeudegrille.model;

/**
 * Created by Ben on 31/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum CellType {
    EMPTY(" - "),
    CHARACTER(" X ");

    String representation;

    CellType(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
