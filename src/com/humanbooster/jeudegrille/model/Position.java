package com.humanbooster.jeudegrille.model;

/**
 * Created by Ben on 31/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Position of(int x, int y) {
        return new Position(x, y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Position incrX() {
        x++;
        return this;
    }

    public Position incrY() {
        y++;
        return this;
    }

    public Position decrX() {
        x--;
        return this;
    }

    public Position decrY() {
        y--;
        return this;
    }

    public int getAxis(Axis axis) {
        return axis.equals(Axis.X) ? x : y;
    }
}
