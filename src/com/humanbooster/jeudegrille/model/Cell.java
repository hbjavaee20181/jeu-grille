package com.humanbooster.jeudegrille.model;

import com.humanbooster.jeudegrille.utils.StringUtils;

import java.util.Objects;

/**
 * Created by Ben on 31/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Cell {

    public static final Cell newEmptyCell() {
        return new Cell(CellType.EMPTY);
    }

    private String identifier;
    private CellType type;

    public Cell(CellType type) {
        this.type = type;
        identifier = StringUtils.generateRandomUUID();
    }

    public Cell() {
        this(CellType.EMPTY);
    }

    public CellType getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setType(CellType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(identifier, cell.identifier) &&
                getType() == cell.getType();
    }

    @Override
    public int hashCode() {

        return Objects.hash(identifier, getType());
    }
}
