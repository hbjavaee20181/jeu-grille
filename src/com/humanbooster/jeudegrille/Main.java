package com.humanbooster.jeudegrille;

import com.humanbooster.jeudegrille.engine.GameEngine;

public class Main {

    public static void main(String[] args) {

        // Singleton
        GameEngine engine = GameEngine.getInstance();

        while (engine.isLaunched()) {
            engine.scanUserInputAndExecute();
        }
    }
}